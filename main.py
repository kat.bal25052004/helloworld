from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///shop.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
db.init_app(app)
with app.app_context():
        db.create_all()



class Item(db.Model): # Инцилизация полей базы данных
    id = db.Column(db.Integer, primary_key=True) # Уникальный номер товара
    title = db.Column(db.String(100), nullable=False) # Название товара
    price = db.Column(db.Integer, nullable=False) # Цена товара
    is_active = db.Column(db.Boolean, default=True) # Наличие товара на складе


@app.route('/')
def home_page():
    #items = Item.query.order_by(Item.price).all() #TODO ||| Ошибка, пока не знаю, как исправить (НЕПРАВИЛЬНЫЙ АРГУМЕНТ)
    return render_template('index.html')#data=items)

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/card')
def card():
    return render_template('card.html')

@app.route('/recom')
def recom():
    return render_template('recom.html')

@app.route('/request', methods=['GET', 'POST'])
def request():
    return render_template('request.html')

@app.route('/recomale')
def recomale():
    return render_template('recomale.html')

@app.route('/create')
def create():
    return render_template('create.html')

if  __name__ == "__main__":
    app.run(debug=True)




